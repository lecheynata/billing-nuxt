// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyCy60D1hmsiOnxtVfgt2jiiI9903_fWUDo',
  authDomain: 'billing-app-b2efa.firebaseapp.com',
  databaseURL: 'https://billing-app-b2efa-default-rtdb.firebaseio.com',
  projectId: 'billing-app-b2efa',
  storageBucket: 'billing-app-b2efa.appspot.com',
  messagingSenderId: '372020241063',
  appId: '1:372020241063:web:c3b7163f682e04aaa2b182',
  measurementId: 'G-ZJTYYCNLVB'
}

export default initializeApp(firebaseConfig)
