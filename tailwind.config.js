const colors = require('tailwindcss/colors')

module.exports = {
  mode: 'jit',
  purge: [
    './components/**/*.vue',
    './pages/**/*.vue',
    './nuxt.config.js',
    './nuxt.config.ts'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        gray: colors.gray,
        indigo: colors.indigo,
        red: colors.red,
        white: colors.white,
        'blue-google': '#039be5'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms')
  ]
}
