export default {
  // https://nuxtjs.org/docs/configuration-glossary/configuration-ssr
  ssr: false,

  // Router Configuration: https://nuxtjs.org/docs/configuration-glossary/configuration-router
  router: {
    prefetchLinks: false
  },

  head: {
    titleTemplate: '%s - Nuxt',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    bodyAttrs: {
      class: 'bg-gray-100'
    }
  },

  // https://nuxtjs.org/docs/configuration-glossary/configuration-modules
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/pwa'
  ],

  pwa: {
    meta: {
      name: 'Billing Nuxt',
      author: 'LECHEYNATA',
      description: 'Personal Expense Management Application',
      theme_color: 'grey',
      lang: 'es'
    },
    manifest: {
      name: 'Billing Nuxt',
      short_name: 'Billing Nuxt',
      description: 'Personal Expense Management Application',
      lang: 'es'
    }
  },

  // https://nuxtjs.org/docs/configuration-glossary/configuration-css
  css: [
    '~/assets/css/tailwind.css'
  ],

  // https://nuxtjs.org/docs/configuration-glossary/configuration-plugins
  plugins: [
    { src: '~/plugins/firebase.client' },
    { src: '~/plugins/firebase.authentication.client' },
    { src: '~/plugins/firebase.database.client' },
    { src: '~/plugins/vue2filters.client' },
    { src: '~/plugins/vueCleaveComponent.client' }
  ],

  // https://nuxtjs.org/docs/configuration-glossary/configuration-build
  build: {
    postcss: {
      plugins: {
        'tailwindcss/nesting': {},
        tailwindcss: {},
        autoprefixer: {}
      }
    }
  },

  // https://nuxtjs.org/docs/configuration-glossary/configuration-runtime-config
  publicRuntimeConfig: {
    firebase: {
      apiKey: process.env.FIREBASE_APIKEY,
      authDomain: process.env.FIREBASE_AUTHDOMAIN,
      databaseURL: process.env.FIREBASE_DATABASE_URL,
      projectId: process.env.FIREBASE_PROJECT_ID,
      storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
      messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
      appId: process.env.FIREBASE_APP_ID,
      measurementId: process.env.FIREBASE_MEASUREMENT_ID
    }
  }
}
