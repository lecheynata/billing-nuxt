export default async ({ $auth, route, redirect }) => {
  await $auth.signedIn().then((user) => {
    if (!user && route.name !== 'login') {
      return redirect('/login')
    } else if (route.name === 'login') {
      return
    }

    console.log(user)
  })
}
