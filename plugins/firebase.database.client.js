// Import the functions you need from the SDKs you need
import { getDatabase, ref, set, push, query, orderByChild, equalTo, startAt, endAt, onValue } from 'firebase/database'
const dayjs = require('dayjs')

export default ({ $firebase }, inject) => {
  // Initialized Instance Firebase
  const database = getDatabase($firebase.getApp)

  const getCollection = (collection = '', queryConstraints) => {
    return new Promise((resolve) => {
      onValue(query(ref(database, collection), orderByChild(queryConstraints.equalTo.key), equalTo(queryConstraints.equalTo.value)), (snapshot) => {
        resolve(snapshot)
      })
    })
  }
  const getMonthlyPayments = (collection = '') => {
    const start = dayjs().date(1).format('YYYY-MM-DD')
    const end = dayjs(start).date(0).add(1, 'month').format('YYYY-MM-DD')

    return new Promise((resolve) => {
      onValue(query(ref(database, collection), orderByChild('date'), startAt(start), endAt(end)), (snapshot) => {
        resolve(snapshot)
      })
    })
  }

  const writeCollection = (collection = '', payload = {}) => {
    return new Promise((resolve, reject) => {
      resolve(set(push(ref(database, collection)), { ...payload }))
    })
  }

  inject('billing', {
    database,
    getMonthlyPayments,
    getCollection,
    writeCollection
  })
}
