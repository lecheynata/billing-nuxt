// Import the functions you need from the SDKs you need
import { getApp, initializeApp } from 'firebase/app'

export default ({ $config }, inject) => {
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: $config.firebase.apiKey,
    authDomain: $config.firebase.authDomain,
    databaseURL: $config.firebase.databaseURL,
    projectId: $config.firebase.projectId,
    storageBucket: $config.firebase.storageBucket,
    messagingSenderId: $config.firebase.messagingSenderId,
    appId: $config.firebase.appId,
    measurementId: $config.firebase.measurementId
  }

  // Initialize firebase instance
  initializeApp(firebaseConfig)

  inject('firebase', {
    getApp: getApp()
  })
}
