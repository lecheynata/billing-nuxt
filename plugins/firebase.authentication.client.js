// Import the functions you need from the SDKs you need
import { getAuth, signInWithEmailAndPassword, signOut as _signOut } from 'firebase/auth'

export default ({ $firebase, redirect }, inject) => {
  // Initialized Instance Firebase
  const auth = getAuth($firebase.getApp)

  const signIn = ({ email, password }) => {
    return new Promise((resolve) => {
      resolve(signInWithEmailAndPassword(auth, email, password))
    })
  }

  const signedIn = () => {
    return new Promise((resolve) => {
      auth.onAuthStateChanged((user) => {
        resolve(user)
      })
    })
  }

  const signOut = async () => {
    await _signOut(auth)
    redirect('/login')
  }

  inject('auth', {
    signIn,
    signedIn,
    signOut
  })
}
